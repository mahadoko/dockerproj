const { testFunction } = require("./testJs")

test('multiply 2 by 2 should return 4', ()=>{
	expect(testFunction(2, 2)).toBe(4);
});

test('multiply 3 by null should return null', ()=>{
	expect(testFunction(3, null)).toBe(null);
});
